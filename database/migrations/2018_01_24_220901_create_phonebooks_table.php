<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonebooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phonebooks', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('kategoriphonebook', array('client','vendor','karyawan','supplier','outsource','bank'));
            $table->string('nama');
            $table->string('telepon')->nullable();
            $table->string('fax')->nullable();
            $table->string('handphone',13)->nullable();
            $table->string('handphone_dua',13)->nullable();
            $table->text('alamat')->nullable();
            $table->text('alamat_dua')->nullable();
            $table->string('kota')->nullable();
            $table->string('kodepos',10)->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phonebooks');
    }
}
