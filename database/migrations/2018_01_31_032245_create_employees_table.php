<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nik')->nullable();
            $table->string('nama_depan');
            $table->string('nama_belakang');
            $table->enum('jeniskelamin', array('pria','perempuan'));
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('alamat');
            $table->string('handphone');
            $table->string('handphone_dua')->nullable();
            $table->enum('status', array('lajang','kawin','cerai'));
            $table->string('email');
            $table->string('ktp');
            $table->string('npmw');
            $table->string('bpjs');
            $table->string('paspor');
            $table->string('foto_karyawan')->nullable();
            $table->string('foto_npmw')->nullable();
            $table->string('foto_bpjs')->nullable();
            $table->string('foto_paspor')->nullable();
            $table->date('masuk_kerja');
            $table->text('skill')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
