-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 15, 2018 at 11:02 AM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.2.0-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinepilot`
--
CREATE DATABASE IF NOT EXISTS `cinepilot` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `cinepilot`;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Aerial Mapping', 'Superadministrator', NULL, '2018-01-31 07:07:46', '2018-01-31 07:07:46'),
(2, 'PIlot', 'Superadministrator', NULL, '2018-01-31 07:07:53', '2018-01-31 07:07:53'),
(3, 'Post Processing', 'Superadministrator', NULL, '2018-01-31 07:08:10', '2018-01-31 07:08:10'),
(4, 'Cameramen', 'Superadministrator', NULL, '2018-01-31 07:24:12', '2018-01-31 07:24:12'),
(5, 'Geographic information System', 'dennissnov', 'dennissnov', '2018-02-01 17:23:07', '2018-02-01 17:23:23');

-- --------------------------------------------------------

--
-- Table structure for table `department_employee`
--

CREATE TABLE `department_employee` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_depan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_belakang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jeniskelamin` enum('pria','perempuan') COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `handphone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `handphone_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('lajang','kawin','cerai') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ktp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `npmw` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bpjs` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paspor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_karyawan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_npmw` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_bpjs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_paspor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `masuk_kerja` date NOT NULL,
  `skill` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2018_01_16_235022_laratrust_setup_tables', 1),
(8, '2018_01_24_220901_create_phonebooks_table', 1),
(11, '2018_01_30_144852_create_departments_table', 2),
(12, '2018_01_31_032245_create_employees_table', 3),
(13, '2018_01_31_041300_create_department_employee_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create-users', 'Create Users', 'Create Users', '2018-01-24 22:59:16', '2018-01-24 22:59:16'),
(2, 'read-users', 'Read Users', 'Read Users', '2018-01-24 22:59:16', '2018-01-24 22:59:16'),
(3, 'update-users', 'Update Users', 'Update Users', '2018-01-24 22:59:16', '2018-01-24 22:59:16'),
(4, 'delete-users', 'Delete Users', 'Delete Users', '2018-01-24 22:59:16', '2018-01-24 22:59:16'),
(5, 'create-acl', 'Create Acl', 'Create Acl', '2018-01-24 22:59:16', '2018-01-24 22:59:16'),
(6, 'read-acl', 'Read Acl', 'Read Acl', '2018-01-24 22:59:16', '2018-01-24 22:59:16'),
(7, 'update-acl', 'Update Acl', 'Update Acl', '2018-01-24 22:59:16', '2018-01-24 22:59:16'),
(8, 'delete-acl', 'Delete Acl', 'Delete Acl', '2018-01-24 22:59:16', '2018-01-24 22:59:16'),
(9, 'read-profile', 'Read Profile', 'Read Profile', '2018-01-24 22:59:16', '2018-01-24 22:59:16'),
(10, 'update-profile', 'Update Profile', 'Update Profile', '2018-01-24 22:59:16', '2018-01-24 22:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(9, 2),
(10, 2),
(9, 3),
(10, 3),
(9, 4),
(10, 4),
(9, 5),
(10, 5),
(9, 6),
(10, 6),
(9, 7),
(10, 7),
(9, 8),
(10, 8);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `phonebooks`
--

CREATE TABLE `phonebooks` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategoriphonebook` enum('client','vendor','karyawan','supplier','outsource','bank') COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `handphone` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `handphone_dua` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci,
  `alamat_dua` text COLLATE utf8mb4_unicode_ci,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kodepos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phonebooks`
--

INSERT INTO `phonebooks` (`id`, `kategoriphonebook`, `nama`, `telepon`, `fax`, `handphone`, `handphone_dua`, `alamat`, `alamat_dua`, `kota`, `kodepos`, `email`, `website`, `keterangan`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(22, 'vendor', 'Dennissnov 23', '021 7333754', '7797225', '081546752005', '0815432154', 'JL.NEPTUNUS TIMUR BLOK K2 NO.39 E MARGAHAYU RAYA, KOTA BANDUNG, JAWA BARAT', 'Seskoad', 'Bandung', '45364', 'dennissnov@gmail.com', 'www.dennissnov.com', 'Ini keterangan sample', 'dennissnov', 'dennissnov', '2018-01-26 10:10:50', '2018-01-30 18:45:01'),
(23, 'karyawan', 'Galang Anarki Rambu', NULL, NULL, '087878377577', NULL, 'Jl. Antene VII No.,6 Radio dalam. Jakarta selatan', NULL, 'Jakarta Selatan', '12140', 'galanganugrahfajar@gmail.com', NULL, NULL, 'dennissnov', 'dennissnov', '2018-01-26 10:29:22', '2018-01-29 21:29:52'),
(24, 'karyawan', 'Ruifak', NULL, NULL, '081567889877', NULL, 'Jl. jalan yuk', NULL, 'Jakarta', '123456', 'ruizaluiza@gmail.com', NULL, NULL, 'dennissnov', NULL, '2018-01-26 10:31:09', '2018-01-26 10:31:09'),
(25, 'supplier', 'AJL', NULL, NULL, '081567889877', NULL, 'Jl. jalan jalan', 'Seskoad', '1244566', NULL, 'bregas.maho@gmail.com', NULL, NULL, 'dennissnov', 'dennissnov', '2018-01-26 10:31:45', '2018-01-31 09:45:32'),
(26, 'bank', 'Ruinui', NULL, NULL, '081567889877', NULL, 'jl. kapiten', NULL, 'Bandung', NULL, 'dennissnov@gmail.com', NULL, NULL, 'dennissnov', NULL, '2018-01-26 10:32:38', '2018-01-26 10:32:38'),
(27, 'supplier', 'Mejita', NULL, NULL, '7797225', NULL, 'jl.. yuk', 'as', 'Cipuralang', NULL, 'bca@bank.com', NULL, NULL, 'dennissnov', 'dennissnov', '2018-01-26 10:33:22', '2018-01-29 21:20:26'),
(28, 'vendor', 'Mezi', NULL, NULL, '081546752005', NULL, 'JL. surga', NULL, 'Heaven', NULL, 'superadministrator@app.com', NULL, NULL, 'dennissnov', 'dennissnov', '2018-01-26 10:34:21', '2018-01-29 21:19:39'),
(29, 'bank', 'Tarmuji', NULL, NULL, '7797225', NULL, 'Jl. kojel kemana', NULL, 'Depok yoo', NULL, 'Tarmuji@gmail.com', NULL, NULL, 'dennissnov', NULL, '2018-01-26 10:35:19', '2018-01-26 10:35:19'),
(30, 'bank', 'dennis novriandi', '021 7333754', NULL, '082465454', NULL, 'bekasi nun jauh disana', NULL, 'Bandung', '454254', 'galanga@gagkl.com', NULL, NULL, 'dennissnov', 'dennissnov', '2018-01-26 10:46:24', '2018-01-29 21:29:10'),
(31, 'client', 'Denniss Nov', '7797225', '7797225', '081546752005', '0815432154', 'JL.NEPTUNUS TIMUR BLOK K2 NO.39 E MARGAHAYU RAYA, KOTA BANDUNG, JAWA BARAT', 'Seskoad', 'Kab Sumedang', '45364', 'dennissnov@gmail.com', 'www.dennissnov.com', NULL, 'dennissnov', 'dennissnov', '2018-01-26 11:24:25', '2018-01-29 21:19:48');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'superadministrator', 'Superadministrator', 'Superadministrator', '2018-01-24 22:59:16', '2018-01-24 22:59:16'),
(2, 'administrator', 'Administrator', 'Administrator', '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(3, 'editor', 'Editor', 'Editor', '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(4, 'author', 'Author', 'Author', '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(5, 'contributor', 'Contributor', 'Contributor', '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(6, 'subscriber', 'Subscriber', 'Subscriber', '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(7, 'user', 'User', 'User', '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(8, 'supporter', 'Supporter', 'Supporter', '2018-01-24 22:59:17', '2018-01-24 22:59:17');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\User'),
(2, 2, 'App\\User'),
(3, 3, 'App\\User'),
(4, 4, 'App\\User'),
(5, 5, 'App\\User'),
(6, 6, 'App\\User'),
(7, 7, 'App\\User'),
(8, 8, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Superadministrator', 'superadministrator@app.com', '$2y$10$wnfVIUzP6JTXvX//WxXaP.bvCkTgXSkOTBo95hnsOHLWcZsHGYjya', 'UWQL3bTERCMNlmp8iaylYq47cEfpD1OiBtELbxq7w0Iy8kuMjcDeVhVwUUw8', '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(2, 'Administrator', 'administrator@app.com', '$2y$10$SFCn7skCoilNjkwRQjSkv.GDc4XWKIh.qzsPSavGdlt8OHvaCjlGK', NULL, '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(3, 'Editor', 'editor@app.com', '$2y$10$KTtk7girSrpymOjvHJQARu6py88aUkQfkBB0wHL4IrFF6KlKtsUT2', NULL, '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(4, 'Author', 'author@app.com', '$2y$10$xdUcFN/ES9Orpg9/wH4Akegm1WlP1c8hcT99Eyij2Hao5kaQZnBgy', NULL, '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(5, 'Contributor', 'contributor@app.com', '$2y$10$bdolhNzfi4.DwrtORBa6keyqV/9A3wKMRli1Q69npdCrXju/d/5Pi', NULL, '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(6, 'Subscriber', 'subscriber@app.com', '$2y$10$HEjoMKfl29IPiPQ3j4Wl9e7znfFrtCp29UP05zLg0jE3nF1Wzs7J.', NULL, '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(7, 'User', 'user@app.com', '$2y$10$Y2k0NzoLTye20YpUbsxThebkmQAH4EkjZ5mnWbtITv.AU7fHCLJG6', NULL, '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(8, 'Supporter', 'supporter@app.com', '$2y$10$lUkb6pDhUZvigeErS88Lg.OjO73lUNeg1Mia4mKAgIBmflN8dNNlu', NULL, '2018-01-24 22:59:17', '2018-01-24 22:59:17'),
(9, 'dennissnov', 'dennissnov@gmail.com', '$2y$10$1cpnAJ2aeg2KvWSOh31t/.NU.jWvfyOn.XWsY1XXKD0RWh0wxxjvu', 'L7nTpCO7dpRrfcMghR1wzVn2NML2gOxftKbsAIwCTqyRostWyIMW6dMCl4XH', '2018-01-25 23:51:48', '2018-01-25 23:51:48'),
(10, 'dennissnov', 'dennissnov@cinepilot.com', '$2y$10$jKJ3DvU17roHeeaToRfpl.4BA0Eud5begfPiTqR9VePbFrGvMRLuW', '0Wn9e2cNTtnfsjNOiWmnBJ5FRoZ9RBnns9GhCBS61GxFskFSIBr9Z6CgfS9h', '2018-02-01 17:19:04', '2018-02-01 17:19:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_employee`
--
ALTER TABLE `department_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `phonebooks`
--
ALTER TABLE `phonebooks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `department_employee`
--
ALTER TABLE `department_employee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `phonebooks`
--
ALTER TABLE `phonebooks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
