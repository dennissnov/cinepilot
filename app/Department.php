<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    protected $fillable =['department','created_by','updated_by'];

    public function employees()
    {
        return $this->belongsToMany(Employee::class);
    }


}


