<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phonebook extends Model
{
    protected $fillable =[
        'kategoriphonebook',
        'nama',
        'telepon',
        'fax',
        'handphone',
        'handphone_dua',
        'alamat',
        'alamat_dua',
        'kota',
        'kodepos',
        'email',
        'website',
        'keterangan',
        'created_by',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'done_at',
        'expired_at'
        
    ];
}
