<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use Yajra\DataTables\DataTables;
class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$department = Department::all();
        return view('humanresource.department.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = [
            'department' => $request['department'],
            'created_by' => $request['created_by'],
        ];
        return Department::create($data);
      //  return redirect('humanresource/department/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $department = Department::find($id);
        return $department;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $department = Department::find($id);
        
        $department->updated_by = $request->updated_by;
        $department->department = $request->department_edit;
        $department->update();

        return $department;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Department::destroy($id);
    }

    public function apiDepartment() 
    {
        $departments = Department::all();

        return DataTables::of($departments)
        ->addColumn('action', function($department){
            return 
             '<a onClick="editForm('. $department->id .')" class="btn btn-primary btn-xs m-r-5"><i class="fa fa-edit"></i></a>'  . 
             '<a onclick="deleteData('. $department->id .')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
        })->make(true);
        
    }
}
