<?php

namespace App\Http\Controllers\humanresource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Department;
use Yajra\DataTables\DataTables;
class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $departments = Department::all();
        return view('humanresource.employee.dashboard',compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function apiEmployee()
    {
        $employees = Employee::all();

        return DataTables::of($employees)
        ->addColumn('action', function($department){
            '<a onClick="showForm('. $department->id .')" class="btn btn-primary btn-xs m-r-5"><i class="fa fa-eye"></i></a>'  . 
            '<a onClick="editForm('. $department->id .')" class="btn btn-primary btn-xs m-r-5"><i class="fa fa-edit"></i></a>'  . 
            '<a onclick="deleteData('. $department->id .')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
        })->make(true);
    }
}
