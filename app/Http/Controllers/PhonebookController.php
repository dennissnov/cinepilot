<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Phonebook;

class PhonebookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        $phonebooks = Phonebook::all();
        return view('inventory.phonebook.dashboard',compact('phonebooks'));
    }
  //  public function phonebook(){
  //      return view('inventory.phonebook');
  //  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return('CREATES IEU');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
  
        $phonebook = new Phonebook;
        $this->validate($request,[
            'kategoriphonebook'=>'required',
            'nama'=>'required',
            'email'=>'required',
            'handphone'=>'required',
            'alamat'=>'required',
        ]);
        //return $request->all();
        $phonebook->created_by = $request->created_by;
        $phonebook->kategoriphonebook = $request->kategoriphonebook;
        $phonebook->nama = $request->nama;
        $phonebook->telepon = $request->telepon;
        $phonebook->fax = $request->fax;
        $phonebook->handphone = $request->handphone;
        $phonebook->handphone_dua = $request->handphone_dua;
        $phonebook->alamat = $request->alamat;
        $phonebook->alamat_dua = $request->alamat_dua;
        $phonebook->kota = $request->kota;
        $phonebook->kodepos = $request->kodepos;
        $phonebook->email = $request->email;
        $phonebook->website = $request->website;
        $phonebook->keterangan = $request->keterangan;
        $phonebook->save();
        return redirect('inventory/phonebook/');
 

            /**
             *          $data = [
            * 'created_by' => $request['created_by'],
            * 'kategoriphonebook' => $request['kategoriphonebook'],
            * 'nama' => $request['nama'],
            * 'telepon' => $request['telepon'],
            * 'fax' => $request['fax'],
            * 'handphone' => $request['handphone'],
            * 'handphone_dua' => $request['handphone_dua'],
            * 'alamat' => $request['alamat'],
            * 'alamat_dua' => $request['alamat_dua'],
            * 'kota' => $request['kota'],
            * 'kodepos' => $request['kodepos'],
            * 'email' => $request['email'],
            * 'website' => $request['website'],
            * 'keterangan' => $request['keterangan'],
         *];
         *return Phonebook::create($data);
             * 
             * 
             * 
             * 
             * 
             * 
             * 
             * 
             * 
             * 
             * 
             * 
             * 
             * 
             */
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $phonebooks = Phonebook::find($id);
        return $phonebooks;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $phonebook = Phonebook::find($id);
        return $phonebook;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       $phonebook = Phonebook::find($id);

       $phonebook->updated_by = $request->updated_by;
       $phonebook->kategoriphonebook = $request->kategoriphonebook_edit;
       $phonebook->nama = $request->nama_edit;
       $phonebook->telepon = $request->telepon_edit;
       $phonebook->fax = $request->fax_edit;
       $phonebook->handphone = $request->handphone_edit;
       $phonebook->handphone_dua = $request->handphone_dua_edit;
       $phonebook->alamat = $request->alamat_edit;
       $phonebook->alamat_dua = $request->alamat_dua_edit;
       $phonebook->kota = $request->kota_edit;
       $phonebook->kodepos = $request->kodepos_edit;
       $phonebook->email = $request->email_edit;
       $phonebook->website = $request->website_edit;
       $phonebook->keterangan = $request->keterangan_edit;
       $phonebook->update();

       return $phonebook;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Phonebook::destroy($id);

    }

    public function apiPhonebook() {
        $phonebooks = Phonebook::all();

        return DataTables::of($phonebooks)
        ->addColumn('action', function($phonebook){
            return '<a onClick="showDetail('. $phonebook->id .')" class="btn btn-primary btn-xs m-r-5"><i class="fa fa-eye"></i></a>'  .
             '<a onClick="editForm('. $phonebook->id .')" class="btn btn-primary btn-xs m-r-5"><i class="fa fa-edit"></i></a>'  . 
             '<a onclick="deleteData('. $phonebook->id .')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
        })->make(true);
    }
}
