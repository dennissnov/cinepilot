<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('inventory')->group (function () {
    Route::get('/', 'InventoryController@index');
    Route::get('dashboard', 'InventoryController@dashboard')->name('inventory.dashboard');
  //  Route::get('phonebook', 'PhonebookController@phonebook')->name('inventory.phonebook');
    Route::resource('phonebook', 'PhonebookController');
    Route::get('apiphonebook', 'PhonebookController@apiPhonebook')->name('apiphonebook');


});

//DEPARTMENT
Route::prefix('humanresource')->group (function () {

    Route::resource('department', 'DepartmentController');
    Route::get('apidepartment', 'DepartmentController@apiDepartment')->name('apidepartment');


    Route::resource('employee', 'humanresource\EmployeeController');
    Route::get('apiemployee', 'humanresource\EmployeeController@apiEmployee')->name('apiemployee');




});
