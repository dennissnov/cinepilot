<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'dennsisnov') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    @include ('_includes.layouts.nav')
    <div id="inventory">
        @include ('_includes.layouts.sidebar')
        <div class="nov-container">
            <div class="nov-row">
                 @yield('content')
                 
                 @yield('modal')
                 @yield('modal2')
            </div>
            @include ('_includes.layouts.footer')  
        </div>

    </div>
    <!-- Scripts -->
    

    <script src="{{ asset('js/app.js') }}"></script>


    @yield('scripts')




</body>

</html>
