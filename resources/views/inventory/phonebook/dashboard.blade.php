@extends('layouts.inventory')

@section('content')
<div class="row">
    <ul class="breadcrumb">
            <li><a href="{{ route('inventory.dashboard')}}">Home</a></li>                    
            <li class="active">Phonebook</li>
        </ul>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default nov-panel">
            <div class="panel-heading">
            <span>PHONEBOOK INPUT</span>
                <a href="#phonebookinput" data-toggle="collapse"><i class="fa fa-window-minimize" style="float:right; color:green;" ></i></a>
            </div>

            <div class="panel-body collapse in" id="phonebookinput">
 
                <form action="{{route('phonebook.store')}}" method="post" id="phonebookform"> 
                {{csrf_field()}}
                    <input type="hidden" name="created_by" value="{{Auth::user()->name}}">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="kategoriphonebook" class="col-sm-2 col-form-label">Kategori</label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="kategoriphonebook" name="kategoriphonebook">
                                        <option value="">Please select category</option>
                                        <option value="client">Client</option>
                                        <option value="vendor">Vendor</option>
                                        <option value="outsource">Outsource</option>
                                        <option value="karyawan">Karyawan</option>
                                        <option value="supplier">Supplier</option>
                                        <option value="bank">Bank</option>
                                    </select>
                                    @if ($errors->has('kategoriphonebook'))<p class="help-block bg-danger"  style="color:orange" >Pilih kategori bro</p> @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="e.g dennisnov" value ="{{ old('nama') }}">
                                    @if ($errors->has('nama'))<p class="help-block bg-danger"  style="color:orange" > masa ga ada namanya, isi dong</p> @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="telepon" class="col-sm-2 col-form-label">Telepon</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="telepon" name="telepon" placeholder="021 7345678" value ="{{ old('telepon') }}">
                                </div>
                                <label for="fax" class="col-sm-2 col-form-label">Fax</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="fax" name="fax" placeholder="021 7345678" value ="{{ old('fax') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="handphone" class="col-sm-2 col-form-label">Hp</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="handphone" name="handphone" placeholder="081546752005" value ="{{ old('handphone') }}">
                                    @if ($errors->has('handphone'))<p class="help-block " style="color:orange" > no hpnya juga diisi dong</p> @endif
                                </div>
                                <label for="handphone_dua" class="col-sm-2 col-form-label">Hp 2</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="handphone_dua" name="handphone_dua" placeholder="081546752005" value ="{{ old('handphone_dua') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control" id="alamat" name="alamat" placeholder="alamat" value ="{{ old('alamat') }}">
                                 <!--   @if ($errors->has('alamat'))<p class="help-block " style="color:orange" > alamat harus diisi {{$errors->first('alamat')}}</p> @endif -->
                                    @if ($errors->has('alamat'))<p class="help-block " style="color:orange" > alamat nya diisi dong</p> @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="alamat_dua" class="col-sm-2 col-form-label">Alamat 2</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control" id="alamat_dua" name="alamat_dua" placeholder="alamat" value ="{{ old('alamat_dua') }}" >
                                </div>
                            </div>

                        </div><!-- END PANEL 1-->
                    
                        <div class="col-lg-6">

                        <div class="form-group row">
                                <label for="kota" class="col-sm-2 col-form-label">Kota</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="kota" name="kota" placeholder="kota" value ="{{ old('kota') }}">
                                </div>
                                <label for="kodepos" class="col-sm-2 col-form-label">Kodepos</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="kodepos" name="kodepos" placeholder="45364" value ="{{ old('kodepos') }}">
                                </div>
                            </div>
                    
                            <div class="form-group row">
                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control" id="email" name="email" placeholder="email@example.com" value ="{{ old('email') }}">
                                    @if ($errors->has('email'))<p class="help-block " style="color:orange" > email wajib isi</p> @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="website" class="col-sm-2 col-form-label">website</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control" id="website"  name="website" placeholder="www.dennissnov.com" value ="{{ old('website') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="keterangan" class="col-sm-2 col-form-label">keterangan</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5"name="keterangan" id="keterangan" value ="{{ old('keterangan') }}"></textarea>
                                </div>
                            </div>
                            <div class="form-group row text-right">
                                  <div class="col-lg-12">
                     
                                     <button type="reset" class="btn btn-danger" name="reset" value='Reset' >Reset</button>
                                     <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>      
                            </div>
                        </div><!-- END PANEL 2-->
                    </div>
                </form>

<!--                 @if (count($errors)>0)

                    @foreach ($errors->all() as $error)
                            {{$error}}
                    @endforeach

                @endif-->






            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 ">
        <div class="panel panel-default  nov-panel ">
            <div class="panel-heading">PHONEBOOK <b>DASHBOARD</b>
            <a href="#phonebookdt" data-toggle="collapse"><i class="fa fa-window-minimize" style="float:right; color:green;" ></i></a>
            </div>


            <div class="panel-body collapse in nov-table" id="phonebookdt">
                <div class="row">
                    <div class="col-lg-12">
                    <table id="phonebook-table" class="table table-striped">
                    <thead>
                        <th width="30">No</th>
                        <th>Nama</th>
                        <th>telepon</th>
                        <th>handphone</th>
                        <th>alamat</th>
                        <th>email</th>
                        <th>website</th>
                        <th>Jenis</th>
                        <th></th
                    </thead>
                    <tbody>


                    </tbody>
                </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
@section('modal')
<div class="modal fade bd-example-modal-lg" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit data <b id="judul_edit"></b></h4>
        </div>
        <form method="post"> 
            {{csrf_field()}} {{ method_field('PATCH')}}
                <input type="hidden" name="updated_by" value="{{Auth::user()->name}}">
                <input type="hidden" name="id" id="id">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="kategoriphonebook_edit" class="col-sm-2 col-form-label">Kategori</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="kategoriphonebook_edit" name="kategoriphonebook_edit">
                                    <option value="">Please select category</option>
                                    <option value="client">Client</option>
                                    <option value="vendor">Vendor</option>
                                    <option value="outsource">Outsource</option>
                                    <option value="karyawan">Karyawan</option>
                                    <option value="supplier">Supplier</option>
                                    <option value="bank">Bank</option>
                                </select>
                                @if ($errors->has('kategoriphonebook'))<p class="help-block bg-danger"  style="color:orange" >Pilih kategori bro</p> @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_edit" class="col-sm-2 col-form-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nama_edit" name="nama_edit" placeholder="e.g dennisnov">
                                @if ($errors->has('nama'))<p class="help-block bg-danger"  style="color:orange" > masa ga ada namanya, isi dong</p> @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="telepon_edit" class="col-sm-2 col-form-label">Telepon</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" id="telepon_edit" name="telepon_edit" placeholder="021 7345678">
                            </div>
                            <label for="fax_edit" class="col-sm-2 col-form-label">Fax</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" id="fax_edit" name="fax_edit" placeholder="021 7345678" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="handphone_edit" class="col-sm-2 col-form-label">Hp</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" id="handphone_edit" name="handphone_edit" placeholder="081546752005" >
                                @if ($errors->has('handphone'))<p class="help-block " style="color:orange" > no hpnya juga diisi dong</p> @endif
                            </div>
                            <label for="handphone_dua_edit" class="col-sm-2 col-form-label">Hp 2</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" id="handphone_dua_edit" name="handphone_dua_edit" placeholder="081546752005" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alamat_edit" class="col-sm-2 col-form-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control" id="alamat_edit" name="alamat_edit" placeholder="alamat" >
                            <!--   @if ($errors->has('alamat'))<p class="help-block " style="color:orange" > alamat harus diisi {{$errors->first('alamat')}}</p> @endif -->
                                @if ($errors->has('alamat'))<p class="help-block " style="color:orange" > alamat nya diisi dong</p> @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alamat_dua_edit" class="col-sm-2 col-form-label">Alamat 2</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control" id="alamat_dua_edit" name="alamat_dua_edit" placeholder="alamat_edit" >
                            </div>
                        </div>

                    </div><!-- END PANEL 1-->
                
                    <div class="col-lg-6">

                    <div class="form-group row">
                            <label for="kota_edit" class="col-sm-2 col-form-label">Kota</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" id="kota_edit" name="kota_edit" placeholder="kota">
                            </div>
                            <label for="kodepos_edit" class="col-sm-2 col-form-label">Kodepos</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" id="kodepos_edit" name="kodepos_edit" placeholder="45364">
                            </div>
                        </div>
                
                        <div class="form-group row">
                            <label for="email_edit" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control" id="email_edit" name="email_edit" placeholder="email@example.com" >
                                @if ($errors->has('email'))<p class="help-block " style="color:orange" > email wajib isi</p> @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="website_edit" class="col-sm-2 col-form-label">website</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control" id="website_edit"  name="website_edit" placeholder="www.dennissnov.com">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="keterangan_edit" class="col-sm-2 col-form-label">keterangan</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="5"name="keterangan_edit" id="keterangan_edit"></textarea>
                            </div>
                        </div>
                        <div class="form-group row text-right">
                            <div class="col-lg-12">
                
                                <button type="reset" class="btn btn-danger" name="reset" value='Reset' >Reset</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>      
                        </div>
                    </div><!-- END PANEL 2-->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection


@section('modal2')
<div class="modal fade bd-example-modal-lg" id="modal-form-show" tabindex="-2" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content nov-modal-content-show" >


            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-7 col-md-offset-3">
                        <h1>Data Detail <b><span id="judul_show"></span></b></h1>

                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-warning pull-right disabled">
                            <b><span id="kategori_show"></span></b>
                        </button>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>

                <div class="col-md-12">
                    <div class="col-md-2"><span class="">nama</span></div>
                    <div class="col-md-10">: <b><span id="nama_show"></span></b></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><span class="">Telepon</span></div>
                    <div class="col-md-10">: <b><span id="telepon_show"></span></b></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><span class="">Fax</span></div>
                    <div class="col-md-10">: <b><span id="fax_show"></span></b></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><span class="">HP</span></div>
                    <div class="col-md-10">: <b><span id="handphone_show"></span></b></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><span class="">HP 2</span></div>
                    <div class="col-md-10">: <b><span id="handphone_dua_show"></span></b></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><span class="">Alamat</span></div>
                    <div class="col-md-10">: <b><span id="alamat_show"></span></b></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><span class="">Alamat 2</span></div>
                    <div class="col-md-10">: <b><span id="alamat_dua_show"></span></b></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><span class="">kota</span></div>
                    <div class="col-md-10">: <b><span id="kota_show"></span></b></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><span class="">kode pos</span></div>
                    <div class="col-md-10">: <b><span id="kodepos_show"></span></b></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><span class="">Email</span></div>
                    <div class="col-md-10">: <b><span id="email_show"></span></b></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><span class="">website</span></div>
                    <div class="col-md-10">: <b><span id="website_show"></span></b></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><span class="">Desc</span></div>
                    <div class="col-md-10">: <b><span id="keterangan_show"></span></b></div>
                </div>
            </div>
        </div>
    </div>
 </div>
@endsection
@section('scripts')

<script type="text/javascript">
            var table = $('#phonebook-table').DataTable({
                        processing : true,
                        serverSide:true,
                        ajax: "{{route('apiphonebook')}}",
                        scrollX:        true,
                        scrollCollapse: true,
                        autoWidth:         true,  
                        paging:         true,
                        columnDefs: [
                            { "width": "5px", "targets": [0] },       
                            { "width": "100px", "targets": [1] },       
     
                            { "width": "400px", "targets": [4] }
                        ],
                        columns : [
                            {data: 'id', name:'id'},
                            {data: 'nama', name:'nama'},
                            {data: 'telepon', name:'telepon'},
                            {data: 'handphone', name:'handphone'},
                            {data: 'alamat', name:'alamat'},
                            {data: 'email', name:'email'},
                            {data: 'website', name:'website'},
                            {data: 'kategoriphonebook', name:'kategoriphonebook'},
                            {data: 'action', name:'action', orderable:false, searchable:false},
                        ]
         });


    </script>
    <script>
            function editForm(id) {
            save_method ='edit';
            $('input[name=_method]').val('PATCH');

            $.ajax({
                url : "{{ url('/inventory/phonebook') }}" + '/' + id + '/edit',
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $('#modal-form').modal('show');
                    $('#judul_edit').text(data.nama);
                    $('#id').val(data.id);
                    $('#kategoriphonebook_edit').val(data.kategoriphonebook);
                    $('#nama_edit').val(data.nama);
                    $('#telepon_edit').val(data.telepon);
                    $('#fax_edit').val(data.fax);
                    $('#handphone_edit').val(data.handphone);
                    $('#handphone_dua_edit').val(data.handphone_dua);
                    $('#alamat_edit').val(data.alamat);
                    $('#alamat_dua_edit').val(data.alamat_dua);
                    $('#kota_edit').val(data.kota);
                    $('#kodepos_edit').val(data.kodepos);
                    $('#email_edit').val(data.email);
                    $('#website_edit').val(data.website);
                    $('#keterangan_edit').val(data.keterangan);
                },
                error : function () {
                    alert("ORA OPOPOPO");
                }
            })
        }
        </script>

        <script>
            $(function(){
                $('#modal-form').on('submit', function (e) {
                    if (!e.isDefaultPrevented()){
                        var id = $('#id').val();
                        if(save_method =='add') url ="{{ url('/inventory/phonebook') }}";
                        else url = "{{ url('/inventory/phonebook') .'/' }}" + id;

                        $.ajax({
                            url : url,
                            type : "POST",
                            data : $('#modal-form form').serialize(),
                            success: function($data) {
                                $('#modal-form').modal('hide');
                                $("#inventory").css({
                                    height: $("#nov-table").height()
                                });
                                table.ajax.reload();
                                swal({
                                        title : 'Sakses',
                                        text : 'Data berhasil diedit',
                                        type : 'success',
                                        timer : '1500'
                                    })
                            },
                            error : function(){
                                alert('ADA YANS SALAH');
                            }
                        });
                        return false
                    }
                })
            })
        </script>
        <script>
            function showDetail(id) {
                $.ajax({
                    url : "{{ url('/inventory/phonebook') }}" + '/' + id,
                    type : "GET",
                    dataType : "JSON",
                    success : function(data) {
                     //   console.log(data);
                        $('#modal-form-show').modal('show');

                    //   console.log(data);    
                        $('#judul_show').text(data.nama);
                        $('#nama_show').text(data.nama);
                        $('#kategori_show').text(data.kategoriphonebook).val(data.kategoriphonebook);
                        $('#telepon_show').text(data.telepon);
                        $('#fax_show').text(data.fax);
                        $('#handphone_show').text(data.handphone);
                        $('#handphone_dua_show').text(data.handphone_dua);
                        $('#alamat_show').text(data.alamat);
                        $('#alamat_dua_show').text(data.alamat_dua);
                        $('#kota_show').text(data.kota);
                        $('#kodepos_show').text(data.kodepos);
                        $('#email_show').text(data.email);
                        $('#website_show').text(data.website);
                        $('#keterangan_show').text(data.keterangan);
                    }
                });
            }
        </script>
        <script>
            function deleteData(id) {
                    var csrf_token = $('meta[name="csrf-token"]').attr('content');
                    swal({
                        title : 'Anda Yakin?',
                        text : "kamu ga akan bisa undo loh",
                        type : 'warning',
                        icon : 'warning',
                        showCancelButton : true,
                        cancelButtonColor : '#d33',
                        confirmButtonColor : '#3085d6',
                        confirmButtonText :'Yeahahah, Delete nih',
                        dangerMode : true,
                    }).then((result)=>{
                        if (result.value) {
                            $.ajax({
                                url : "{{url('/inventory/phonebook')}}" + '/' + id,
                                type : "POST",
                                data : {'_method' : 'DELETE', '_token' : csrf_token},
                                success : function(data) {
                                    table.ajax.reload();
                                    //console.log(data);
                                    swal({
                                        title : 'Sakses',
                                        text : 'data berhasil dihapus',
                                        type : 'success',
                                        timer : '1500'
                                    })
                                },
                                error : function () {
                                    swal({
                                        title : 'Oops',
                                        text : 'Ad yang salah nih',
                                        type : 'error',
                                        timer : '1500'
                                    })
                                }
                            });
                        } else if (result.dismiss === 'cancel'){
                            swal(
                                'Cancelled',
                                'Data Aman ga jadi Delete',
                                'error'
                            )
                        }
                    })
            }           
        
        </script>
@endsection