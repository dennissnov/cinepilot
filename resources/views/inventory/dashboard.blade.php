@extends('layouts.inventory')

@section('content')

<div class="row">
    <ul class="breadcrumb">
        <li><a href="{{ route('inventory.dashboard')}}">Home</a></li>                    
        <li class="active">Dashboard</li>
    </ul>
  
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                Welcome back <b>{{Auth::user()->name }}</b>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                Welcome back <b>{{Auth::user()->name }}</b>
            </div>
        </div>
    </div>
</div>

@endsection
