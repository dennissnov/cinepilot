<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body class="page-welcome">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                    @auth

                    @else
                    <input type="checkbox" class="check" id="checked">
                    <label class="btn-dpn menu-btn" for="checked">
                        LOGIN
                    </label>
                    <input type="checkbox" class="check2" id="checked2">
                    <label class="btn-dpn menu-btn2" for="checked2">
                        REGISTER
                    </label>
                    @endauth
            @endif


            <label class="close-menu" for="checked">NOC</label>
            <nav class="drawer-menu">

                    <div class="panel-body">
                    <img class ="imgbg" src="{{asset('images/logo_cinepilot.png')}}" alt="">
                            
                       <h1 class="p-b-20"> CINEPILOT<b> LOGIN</b> </h1>
                        @include ('auth._login')
                     </div>
            </nav>

            <label class="close-menu2" for="checked2">KLOSE REG</label>
            <nav class="drawer-menu2">

                    <div class="panel-body">
                    <img class ="imgbg" src="{{asset('images/logo_cinepilot.png')}}" alt="">
                            
                       <h1> CINEPILOT<b> REGISTER</b> </h1>
                        @include ('auth._register')
                     </div>
            </nav>
            <div class="contents">
                <div class="contents_inner">
                @if(Route::has('login'))
                    @auth
                        <h1>Welcome Back, <b>{{ Auth::user()->name }}</b></h1>
                    @endauth
                @endif
                    <a href="{{route('inventory.dashboard') }}">
                        <img class="imgbg" src="{{asset('images/logo_cinepilot.png')}}" alt="">
                    </a>
                    <h1>CINEPILOT INVENTORY SYSTEM</h1>
                    <p>copyright @ 2017 | cinepilot | dennissnov </p>
                </div>
            </div>
        </div>
    </body>
</html>
