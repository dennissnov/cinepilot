<nav class="nov-sidebar nov-open" >
    <div class="nov-sidebar-container">
        <ul class="nov-sidebar-list">
            <li class="nov-sidebar-profile">
                <div class="sidebar-profile-container">
                    <ul>
                        <li class="sidebar-profile-photo">
                            <img src="{{ asset('images/logo_fixed.jpg') }}" />
                        </li>
                        <li class="sidebar-profile-name">
                            <span>{{strtoupper(Auth::user()->name )}}</span>
                        </li>
                        <li class="sidebar-profile-email">
                            <span>{{ Auth::user()->email }}</span>
                        </li>
                    </ul>
                </div>
            </li>
               
            <li class="nov-sidebar-menu ">
                <a href="{{ route('inventory.dashboard')}}"><i class="fa fa-home"></i><span>DASHBOARD</span></a>
            </li>
            <li class="nov-sidebar-menu">
               <!-- <a class="accordion-toggle collapsed toggle-switch" data-toggle="collapse" href="#submenu-1"> -->
                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#submenu-1">
                    <i class="fa fa-camera-retro"></i>
                    <span>INVENTORY</span>
                </a>
               <!--  <ul id="submenu-1" class="panel-collapse collapse panel-switch" role="menu">-->
                <ul id="submenu-1" class="collapse" role="menu">
                    <li class="nov-sidebar-menu"><a href="#"><i class="fa fa-cube"></i><span>Category</span></a></li>
                    <li class="nov-sidebar-menu"><a href="#"><i class="fa fa-truck"></i><span>Barang Masuk</span></a></li>
                    <li class="nov-sidebar-menu"><a href="#"><i class="fa fa-plane"></i><span>Barang Keluar</span></a></li>
                    <li class="nov-sidebar-menu"><a href="#"><i class="fa fa-building"></i><span>Data Barang</span></a></li>
                    <li class="nov-sidebar-menu"><a href="#"><i class="fa fa-chain-broken"></i><span>Barang Rusak</span></a></li>
                    <li class="nov-sidebar-menu"><a href="#"><i class="fa fa-handshake-o"></i><span>Pengajuan Barang</span></a></li>
                </ul>
            </li>
            <li class="nov-sidebar-menu" >
                <a class="accordion-toggle collapsed" id="nov-sidebar-hrd" data-toggle="collapse" href="#submenu-2">
                    <i class="fa fa-grav"></i>
                    <span>HUMAN RESOURCE</span>
                </a>
               <!--  <ul id="submenu-1" class="panel-collapse collapse panel-switch" role="menu">-->
                <ul id="submenu-2" class="collapse" role="menu">
                    <li class="nov-submenu nov-sidebar-menu"><a href="{{ route('department.index')}}"><i class="fa fa-sitemap"></i><span>Department</span></a></li>
                    <li class="nov-submenu nov-sidebar-menu"><a href="{{ route('employee.index')}}"><i class="fa fa-resistance"></i><span>Employee</span></a></li>
                    <li class="nov-submenu nov-sidebar-menu"><a href="#"><i class="fa fa-users"></i><span>Outsource</span></a></li>
                </ul>
            </li>
            <li class="nov-sidebar-menu">
                <a href="{{ route('phonebook.index')}}"><i class="fa fa-address-book"></i><span> PHONEBOOK</span></a>
            </li>

        </ul>
    </div>
</nav>