@extends('layouts.inventory')
@section('content')

<div class="row">
    <ul class="breadcrumb">
            <li><a href="{{ route('inventory.dashboard')}}">Home</a></li>                    
            <li class="active">employee</li>
        </ul>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default nov-panel">
            <div class="panel-heading">
            <span>employee INPUT</span>
                <a href="#employeeinput" data-toggle="collapse"><i class="fa fa-window-minimize" style="float:right; color:green;" ></i></a>
            </div>

            <div class="panel-body collapse in" id="employeeinput">
 
                <form action="{{route('employee.store')}}" method="post" id="employeeform"> 
                {{csrf_field()}}
                    <input type="hidden" name="created_by" value="{{Auth::user()->name}}">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="kategoriemployee" class="col-sm-2 col-form-label">Department</label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="kategoriemployee" name="kategoriemployee">

                                        @foreach( $departments as $department)
                                                <option value="{{$department->id}}">{{$department->department}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('kategoriemployee'))<p class="help-block bg-danger"  style="color:orange" >Pilih kategori bro</p> @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nama_depan" class="col-sm-2 col-form-label">nama_depan</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="nama_depan" name="nama_depan" placeholder="e.g dennisnov" value ="{{ old('nama') }}">
                                    @if ($errors->has('nama_depan'))<p class="help-block bg-danger"  style="color:orange" > masa ga ada namanya, isi dong</p> @endif
                                </div>
                                <label for="nama_belakang" class="col-sm-2 col-form-label">nama_belakang</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="nama_belakang" name="nama_belakang" placeholder="nama_belakang" value ="{{ old('nama_belakang') }}">
                                </div>                                
                            </div>
                            <div class="form-group row">
                                <label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="e.g dennisnov" value ="{{ old('nama') }}">
                                    @if ($errors->has('nama_depan'))<p class="help-block bg-danger"  style="color:orange" > masa ga ada namanya, isi dong</p> @endif
                                </div>
                                <label for="tanggal_lahir" class="col-sm-2 col-form-label datepicker">Tanggal Lahir</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="tanggal_lahir" value ="{{ old('nama_belakang') }}">
                                </div>                                
                            </div>
                            <div class="form-group row">
                                <label for="telepon" class="col-sm-2 col-form-label">Telepon</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="telepon" name="telepon" placeholder="021 7345678" value ="{{ old('telepon') }}">
                                </div>
                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="email" name="email" placeholder="081546752005" value ="{{ old('email') }}">
                                    @if ($errors->has('handphone'))<p class="help-block " style="color:orange" > no hpnya juga diisi dong</p> @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="handphone" class="col-sm-2 col-form-label">Hp 2</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="handphone" name="handphone" placeholder="081546752005" value ="{{ old('handphone') }}">
                                </div>
                                <label for="handphone_dua" class="col-sm-2 col-form-label">Hp 2</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="handphone_dua" name="handphone_dua" placeholder="081546752005" value ="{{ old('handphone_dua') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control" id="alamat" name="alamat" placeholder="alamat" value ="{{ old('alamat') }}">
                                 <!--   @if ($errors->has('alamat'))<p class="help-block " style="color:orange" > alamat harus diisi {{$errors->first('alamat')}}</p> @endif -->
                                    @if ($errors->has('alamat'))<p class="help-block " style="color:orange" > alamat nya diisi dong</p> @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="alamat_dua" class="col-sm-2 col-form-label">Alamat 2</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control" id="alamat_dua" name="alamat_dua" placeholder="alamat" value ="{{ old('alamat_dua') }}" >
                                </div>
                            </div>

                        </div><!-- END PANEL 1-->
                    
                        <div class="col-lg-6">

                        <div class="form-group row">
                                <label for="kota" class="col-sm-2 col-form-label">Kota</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="kota" name="kota" placeholder="kota" value ="{{ old('kota') }}">
                                </div>
                                <label for="kodepos" class="col-sm-2 col-form-label">Kodepos</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" id="kodepos" name="kodepos" placeholder="45364" value ="{{ old('kodepos') }}">
                                </div>
                            </div>
                    
                            <div class="form-group row">
                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control" id="email" name="email" placeholder="email@example.com" value ="{{ old('email') }}">
                                    @if ($errors->has('email'))<p class="help-block " style="color:orange" > email wajib isi</p> @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="website" class="col-sm-2 col-form-label">website</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control" id="website"  name="website" placeholder="www.dennissnov.com" value ="{{ old('website') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="keterangan" class="col-sm-2 col-form-label">keterangan</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5"name="keterangan" id="keterangan" value ="{{ old('keterangan') }}"></textarea>
                                </div>
                            </div>
                            <div class="form-group row text-right">
                                  <div class="col-lg-12">
                     
                                     <button type="reset" class="btn btn-danger" name="reset" value='Reset' >Reset</button>
                                     <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>      
                            </div>
                        </div><!-- END PANEL 2-->
                    </div>
                </form>
                </div>
        </div>
    </div>
</div>
<!--                 @if (count($errors)>0)

                    @foreach ($errors->all() as $error)
                            {{$error}}
                    @endforeach

                @endif-->
<div class="row">
    <div class="col-md-12 ">
        <div class="panel panel-default  nov-panel ">
            <div class="panel-heading">employee <b>DASHBOARD</b>
                <a href="#employeedt" data-toggle="collapse"><i class="fa fa-window-minimize" style="float:right; color:green;" ></i></a>
            </div>
            <div class="panel-body collapse in nov-table" id="employeedt">
                <div class="row">
                    <div class="col-lg-12">
                    <table id="employee-table" class="table table-striped">
                    <thead>
                        <th width="30">No</th>
                        <th>Nama</th>
                        <th>telepon</th>
                        <th>handphone</th>
                        <th>alamat</th>
                        <th>email</th>
                        <th>website</th>
                        <th>Jenis</th>
                        <th></th
                    </thead>
                    <tbody>


                    </tbody>
                </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')

<script>

    $( function() {
        $( "#tanggal_lahir" ).datetimepicker();
    } );

    $( function() {
        $( "#tanggal_masuk" ).datetimepicker();
    } );

</script>


@endsection