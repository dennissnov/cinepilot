@extends('layouts.inventory')

@section('content')
<div class="row">
    <ul class="breadcrumb">
            <li><a href="{{ route('inventory.dashboard')}}">Home</a></li>                    
            <li><a href="#">Human Resource</a></li>                    
            <li class="active">department</li>
        </ul>
</div>
<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel panel-default nov-panel">
            <div class="panel-heading">
            <span>DEPARTMENT INPUT</span>
                <a href="#departmentinput" data-toggle="collapse"><i class="fa fa-window-minimize" style="float:right; color:green;" ></i></a>
            </div>
            <div class="panel-body collapse in" id="departmentinput">
                <form method="post" id="departmentform" > 
                {{csrf_field()}} {{ method_field('POST')}}
                    <input type="hidden" name="created_by" value="{{Auth::user()->name}}">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label for="department" class="col-sm-2 col-form-label">department</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="department" name="department" placeholder="e.g karyawan" value ="{{ old('department') }}">
                                    @if ($errors->has('department'))<p class="help-block bg-danger"  style="color:orange" > masa ga ada department, isi dong</p> @endif
                                </div>
                            </div>
                            <div class="form-group row text-right">
                                  <div class="col-lg-12">
                     
                                     <button type="reset" class="btn btn-danger" name="reset" value='Reset' >Reset</button>
                                     <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>      
                            </div>
                        </div><!-- END PANEL 1-->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-6 col-lg-offset-3">
        <div class="panel panel-default  nov-panel ">
            <div class="panel-heading">DEPARTMENT <b>DASHBOARD</b>
            <a href="#departmentdt" data-toggle="collapse"><i class="fa fa-window-minimize" style="float:right; color:green;" ></i></a>
            </div>


            <div class="panel-body collapse in nov-table" id="departmentdt">
                <div class="row">
                    <div class="col-lg-12">
                    <table id="department-table" class="table table-striped">
                        <thead>
                            <th width="30">No</th>
                            <th>Nama Department</th>
                            <th></th
                        </thead>
                    <tbody>


                    </tbody>
                </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection


@section('modal')
<div class="modal fade bd-example-modal-sm" id="modal-department-edit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit data <b id="judul_edit"></b></h4>
        </div>
        <form method="post" id="modal-form-department"> 
        {{csrf_field()}} {{ method_field('PATCH')}}
                <input type="hidden" name="updated_by" value="{{Auth::user()->name}}">
                <input type="hidden" name="id" id="id">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label for="department_edit" class="col-sm-2 col-form-label">department</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="department_edit" name="department_edit">
                                @if ($errors->has('department_edit'))<p class="help-block bg-danger"  style="color:orange" > masa ga ada namanya, isi dong</p> @endif
                            </div>
                        </div>

                        <div class="form-group row text-right">
                                  <div class="col-lg-12">
                     
                                     <button type="reset" class="btn btn-danger" name="reset" value='Reset' >Reset</button>
                                     <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>      
                            </div>
                    </div><!-- END PANEL 1-->            
                </div>
            </form>
        </div>
    </div>
</div>
@endsection



@section('scripts')
<script type="text/javascript">
            var table = $('#department-table').DataTable({
                processing : true,
                serverSide:true,
                ajax: "{{route('apidepartment')}}",

               // columnDefs: [
             //       { "width": "5px", "targets": [0] },       
             //       { "width": "100px", "targets": [1] },       
             //   ],
                columns : [
                    {data: 'id', name:'id'},
                    {data: 'department', name:'department'},
                    {data: 'action', name:'action', orderable:false, searchable:false},
                ]
            });
            
</script>
<script>


$(function(){
                $('#departmentform').on('submit', function (e) {
                    if (!e.isDefaultPrevented()){
                        $('input[name=_method]').val('POST');
                        var id = $('#id').val();
                        $.ajax({
                            url : "{{ url ('humanresource/department')}}",
                            type : "POST",
                            data : $("#departmentform").serialize(),
                            success : function($data){
                                console.log('BERHASIL');
                               // window.location.href = "http://cinepilot.test/humanresource/department";
                                table.ajax.reload();
                                swal({
                                        title : 'Sakses',
                                        text : 'Data berhasil diedit',
                                        type : 'success',
                                        timer : '1500'
                                    })
                            },
                            error : function() {
                                alert('ADA YANG SALAH SUBMIT');
                                console.log(data);
                                
                            }
                        });
                        return false;
                    }
                });
            });


            function deleteData(id) {
                    var csrf_token = $('meta[name="csrf-token"]').attr('content');
                    swal({
                        title : 'Anda Yakin?',
                        text : "kamu ga akan bisa undo loh",
                        type : 'warning',
                        icon : 'warning',
                        showCancelButton : true,
                        cancelButtonColor : '#d33',
                        confirmButtonColor : '#3085d6',
                        confirmButtonText :'Yeahahah, Delete nih',
                        dangerMode : true,
                    }).then((result)=>{
                        if (result.value) {
                            $.ajax({
                                url : "{{url('/humanresource/department')}}" + '/' + id,
                                type : "POST",
                                data : {'_method' : 'DELETE', '_token' : csrf_token},
                                success : function(data) {
                                    table.ajax.reload();
                                    console.log(data);
                                    swal({
                                        title : 'Sakses',
                                        text : 'data berhasil dihapus',
                                        type : 'success',
                                        timer : '1500'
                                    })
                                },
                                error : function () {
                                    swal({
                                        title : 'Oops',
                                        text : 'Ad yang salah nih',
                                        type : 'error',
                                        timer : '1500'
                                    })
                                }
                            });
                        } else if (result.dismiss === 'cancel'){
                            swal(
                                'Cancelled',
                                'Data Aman ga jadi Delete',
                                'error'
                            )
                        }
                    })
            }          
        //------------------------------------------------------------------------------
            function editForm(id) {
                //  save_method ='edit';
                $('input[name=_method]').val('PATCH');

                $.ajax({
                    url : "{{ url('/humanresource/department') }}" + '/' + id + '/edit',
                    type: "GET",
                    dataType: "JSON",
                    success: function(data) {
                        $('#modal-department-edit').modal('show');
                        $('#judul_edit').text(data.department);
                        $('#id').val(data.id);
                        $('#department_edit').val(data.department);
                    },
                    error : function () {
                        alert("ORA OPOPOPO");
                        console.log(id);
                    }
                })
            }


            $(function(){
                $('#modal-form-department').on('submit', function (e) {
                    if (!e.isDefaultPrevented()){
                        var id = $('#id').val();
                        $.ajax({
                            url : "{{ url('/humanresource/department') .'/' }}" + id,
                            type : "POST",
                            data : $('#modal-form-department').serialize(),
                            success: function($data) {
                                $('#modal-department-edit').modal('hide');
                                console.log('ini data' + $data);

                                table.ajax.reload();
                                swal({
                                        title : 'Sakses',
                                        text : 'Data berhasil diedit',
                                        type : 'success',
                                        timer : '1500'
                                    })
                            },
                            error : function(){
                                alert('ADA YANS SALAH');
                            }
                        });
                        return false
                    }
                })
            })

            
</script>
@endsection